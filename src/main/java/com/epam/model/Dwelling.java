package com.epam.model;

public abstract class Dwelling {
    private String address;
    private int costOfRenting;
    private int area;
    private int numberOfRooms;
    private int dinstanceToSchool;
    private int dinstanceToKindergarten;

     Dwelling(String address, int costOfRenting, int area, int numberOfRooms, int dinstanceToSchool,
                    int dinstanceToKindergarten) {
        this.address = address;
        this.costOfRenting = costOfRenting;
        this.area = area;
        this.numberOfRooms = numberOfRooms;
        this.dinstanceToSchool = dinstanceToSchool;
        this.dinstanceToKindergarten = dinstanceToKindergarten;
    }

    public final int getCostOfRenting() {
        return costOfRenting;
    }

    public final int getArea() {
        return area;
    }

    public final int getNumberOfRooms() {
        return numberOfRooms;
    }

    public final int getDinstanceToSchool() {
        return dinstanceToSchool;
    }

    public final int getDinstanceToKindergarten() {
        return dinstanceToKindergarten;
    }

    public final String getAddress() {
        return address;
    }

    public String toString() {
        return "Address: " + address + "Cost: " + costOfRenting + "Area: " + area + "\tNumber of rooms: " + numberOfRooms;
    }
}
