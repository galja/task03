package com.epam.model;

import java.util.ArrayList;

public class Catalogue {

    private ArrayList<Flat> flats;
    private ArrayList<House> houses;
    private ArrayList<Penthouse> penthouses;

    public Catalogue() {
        flats = new ArrayList<Flat>();
        houses = new ArrayList<House>();
        penthouses = new ArrayList<Penthouse>();
        addFlats();
        addHouses();
        addPenthouses();
    }

    public final ArrayList<Flat> getFlats() {
        return flats;
    }

    public final ArrayList<House> getHouses() {
        return houses;
    }

    public final ArrayList<Penthouse> getPenthouses() {
        return penthouses;
    }

    private void addFlats() {
        flats.add(new Flat("Kyiv", 300, 40,
                2, 3, 5, 8));
        flats.add(new Flat("Lviv", 800, 90,
                3, 1, 1, 4));
        flats.add(new Flat("Lviv", 500, 60,
                2, 5, 1, 2));
    }

    private void addHouses() {
        houses.add(new House("Ternopil", 3000, 260, 6,
                10, 5, 2, "Garage, swimming pool"));
        houses.add(new House("Lviv", 800, 150, 4,
                12, 5, 1, "Garage"));
    }

    private void addPenthouses() {
        penthouses.add( new Penthouse("Lviv", 900, 100, 4,
                3, 2, 14, "Private entrance"));
        penthouses.add(new Penthouse("Lviv", 1200, 200, 5,
                4, 1, 11, "Terrace, oversized windows"));
    }
}
