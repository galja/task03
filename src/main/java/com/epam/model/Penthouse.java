package com.epam.model;

public class Penthouse extends Flat {
    private String amenities;

    public Penthouse(String address, int costOfRenting, int area, int numberOfRooms, int dinstanceToSchool,
                     int dinstanceToKindergarten, int floor, String amenities) {
        super(address, costOfRenting, area, numberOfRooms, dinstanceToSchool, dinstanceToKindergarten, floor);
        this.amenities = amenities;
    }

    public final String getAmenities() {
        return amenities;
    }

    @Override
    public String toString() {
        return "Address: " + getAddress() + "\tPrice: " + getCostOfRenting() +
                "\tArea: " + getArea() + "\tFloor: " + getFloor()+"\tAmenities: "+amenities+
                "\tDistance to school: "+getDinstanceToSchool();
    }
}

