package com.epam.smc_sender;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
public class ExampleSMS {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "AC69875ab874af9708c620a1ab6f3b99ca";
    public static final String AUTH_TOKEN = "f9bacc9ac3b8c24873e7bf2c8cef567b";
    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380996560756"), /*my phone number*/
                        new PhoneNumber("+16232320542"), str) .create(); /*attached to me number*/
    }
}